package prepay.prepay.view;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import prepay.prepay.R;

public class GenerateCode extends ActionBarActivity {

    private Button continueToOrderDetails;
    private TextView codeDisplay;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_code);

        continueToOrderDetails = (Button) findViewById(R.id.continue_to_order_details);
        codeDisplay = (TextView) findViewById(R.id.code_to_display);

        //TODO: need to get code from server

        codeDisplay.setText("ITAI511");

        continueToOrderDetails.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                Intent order_details = new Intent(getBaseContext(), OrderDetails.class);
                startActivity(order_details);                 //start Order Details activity
            }
        });




    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_generate_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
