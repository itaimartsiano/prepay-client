package prepay.prepay.view;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import prepay.prepay.R;
import prepay.prepay.util.Manager;

/**
 * Created by itai marts on 14/07/2015.
 */
public class DialogJoinTable extends DialogFragment implements View.OnClickListener{

    private Button continueBtn;
    private Button cancelBtn;
    private EditText inputText;
    private String restName;
    private String nickname;
    private ProgressDialog progress;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {

        View myView = inflater.inflate(R.layout.dialog_open_join_table,null);

        getDialog().setTitle("Insert your friend nickname");

        restName = getArguments().getString("restName");
        inputText = (EditText) myView.findViewById(R.id.input_table_name);
        continueBtn = (Button) myView.findViewById(R.id.continue_btn_join);
        cancelBtn= (Button) myView.findViewById(R.id.cancel_btn_join);
        continueBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("מאמת..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        setCancelable(false);
        return myView;
    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.continue_btn_join)
        {
            nickname = inputText.getText().toString();
            if (nickname.length()<1){
                nicknameIsNotValid();
            }
            else{

                String [] params = {restName,nickname};
                Log.i("ccccccccccccccccccccccccccccccccccccccc",restName+"-"+nickname);
                progress.show();
                Manager.getInstance().getConnectionHandler().joinTableTask(params,this);
            }
        }else
        {
            dismiss();
        }
    }



    public void seeOrderDetails(){
        Intent orderDetails = new Intent(getActivity(),OrderDetails.class);
        orderDetails.putExtra("restName",restName);
        orderDetails.putExtra("nickName",nickname);
        //TODO:need to retrieve and send the resurant name and else
        progress.dismiss();
        startActivity(orderDetails);                 //start generate code activity
        dismiss();
    }

    public void nicknameIsNotValid(){
        Toast.makeText(getActivity(), "Nickname is not valid", Toast.LENGTH_LONG).show();
    }
}
