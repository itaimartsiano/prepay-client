package prepay.prepay.view;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import prepay.prepay.R;

public class ChooseOrJoin extends ActionBarActivity {

    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_choose_or_join);
    }


    /**
     * call by clicking on chooseRestuarant button and move you to choose Restaurant activity
     * @param view
     */
    public void chooseRestuarant(View view){
        Intent chooseReturant = new Intent(getBaseContext(), ChooseRestuarant.class);
        startActivity(chooseReturant);                 //change content view to choose resturant
    }


    /**
     * call by join table button and move you to choose restuarant screen but with param true which
     * symbol the activity to ask join table
     * @param view
     */
    public void joinTable(View view){
        Intent chooseReturant = new Intent(getBaseContext(), ChooseRestuarant.class);
        chooseReturant.putExtra("joinTable", true);
        startActivity(chooseReturant);                 //change content view to choose resturant
    }











    //----------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_or_join, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
