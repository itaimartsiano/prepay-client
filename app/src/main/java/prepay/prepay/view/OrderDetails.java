package prepay.prepay.view;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import prepay.prepay.R;
import prepay.prepay.Services.UpdateService;
import prepay.prepay.restuarantObjects.Product;
import prepay.prepay.util.Connectivity.ConnectionHandler;
import prepay.prepay.util.Manager;

public class OrderDetails extends ActionBarActivity implements AdapterView.OnItemClickListener, SeekBar.OnSeekBarChangeListener {

    private ListView mainListView ;
    private HashMap <Integer,Product> products = new HashMap<Integer,Product>();
    private ArrayAdapter<Product> listAdapter ;
    private SeekBar tipBar;
    private int tip = 10;
    private double total;
    private double subTotal;
    private TextView subTotalTextView;
    private TextView totalTextView;
    private TextView tipTextView;
    private String restName;
    private String nickName;


    //broadcast reciever on recieve method and filter
    public static final String broadcastFilter = ".Services.UpdateService";
    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(broadcastFilter)) {
                HashMap <Integer,Product> hash = (HashMap<Integer, Product>) intent.getSerializableExtra("hash");
                updateListAdapter(hash);
            }
        }
    };


    //TODO:synchronize on check and update list

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        tipBar = (SeekBar) findViewById(R.id.seekBar);
        subTotalTextView = (TextView) findViewById(R.id.sub_total_text);
        totalTextView = (TextView) findViewById(R.id.total_text);
        tipTextView = (TextView) findViewById(R.id.tip_text);
        restName = getIntent().getStringExtra("restName");
        nickName = getIntent().getStringExtra("nickName");

        // Find the ListView resource.
        mainListView = (ListView) findViewById(R.id.main_list_product);

        //move the products hashmap to arraylist
        ArrayList <Product> productList = new ArrayList<Product>();
        productList.addAll(products.values());

        // Set our custom array adapter as the ListView's adapter.
        listAdapter = new ProductArrayAdapter(this, productList);
        mainListView.setAdapter(listAdapter);

        // When item is tapped, toggle checked properties of CheckBox and product.
        mainListView.setOnItemClickListener(this);

        tipBar.setOnSeekBarChangeListener(this);

        Intent intent = new Intent(this, UpdateService.class);
        intent.putExtra("restName",restName);
        intent.putExtra("nickName",nickName);
        startService(intent);

        //get the broadcast manager and regiester the bReciever with relevant filter
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(broadcastFilter);
        bManager.registerReceiver(bReceiver, intentFilter);

    }




    //list view listener
    public void onItemClick(AdapterView<?> parent, View item, int position, long id) {

        Product product = listAdapter.getItem(position);

        //set the product checkbox field to true/false
        product.toggleChecked();

        //set the checkbox view with V/not V
        ProductViewHolder viewHolder = (ProductViewHolder) item.getTag();
        viewHolder.getCheckBox().setChecked(product.isChecked());

        //update total price
        if(product.isChecked()){
            subTotal = subTotal + product.getPrice();
            updatePrice();
        }else {
            subTotal = subTotal - product.getPrice();
            updatePrice();
        }
    }



    //seekbar listener
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        tip = progress;
        Log.i("TIIIIIIIIIIPPPP", String.valueOf(progress));
        updatePrice();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}




    //updating total price method
    private void updatePrice(){
        double newTip = tip*subTotal/100;
        subTotalTextView.setText("Sub-total: "+Double.toString(subTotal)+ " $");
        tipTextView.setText("Tip: "+Integer.toString(tip)+" %");
        total = subTotal+newTip;
        totalTextView.setText("Total: "+Double.toString(total)+" $");
    }



    //update list view
    //service check using the connection handler if there is new version of products, if there is this method would call
    public void updateListAdapter(HashMap<Integer,Product> products){

        this.products = checkOldProductsMarked(products);
        listAdapter.clear();
        listAdapter.addAll(this.products.values());
        updatePrice();
        listAdapter.notifyDataSetChanged();

    }



    //check if something in the prev hashmap was checked
    public HashMap<Integer,Product> checkOldProductsMarked(HashMap<Integer, Product> newHashmapProducts){

        for (Integer key : newHashmapProducts.keySet()) {
            Log.i("check old products", " - key number"+ key);
            if (products.get(key) != null && products.get(key).isChecked() == true && newHashmapProducts.get(key) != null){
                newHashmapProducts.get(key).setChecked(true);
                Log.i("Order Details -", products.get(key).getName()+"was checked!");
            }
        }

        //check all the prev list if there are products which was marked but not include in the new list
        double newSubTotal = 0;
        for (Integer key : newHashmapProducts.keySet()) {
            if (newHashmapProducts.get(key).isChecked() == true){
                newSubTotal += newHashmapProducts.get(key).getPrice();
            }
        }
        subTotal = newSubTotal;

        return newHashmapProducts;

    }




    public void payOrder(View view){

        StringBuilder products_checked = new StringBuilder();

        //check if all products was payed
        int i = 0;

        for (Integer key : products.keySet()) {
            if (products.get(key).isChecked() == true){
                products_checked.append(key).append("-");
                i++;
            }
        }

        doPaymentViaConnectionHandler(products_checked.toString());


    }






    private void doPaymentViaConnectionHandler(String productOrderNumbers){

        Manager manager = Manager.getInstance();
        ConnectionHandler connectionHandler = manager.getConnectionHandler();

        String [] params = new String[3];
        params[0] = restName;
        params[1] = nickName;
        params[2] = productOrderNumbers;

        connectionHandler.doPayment(params, this);
    }




    public void paymentRecievedSuccessfully(){
        FragmentManager fragmentManager = getFragmentManager();

        Bundle bundle = new Bundle();

        bundle.putBoolean("all was payed", true);
        bundle.putBoolean("all was payed", false);

        //show dialog
        DialogThanksForPaying dialog = new DialogThanksForPaying();
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "paymeמt recieved");

    }




















    //list view and adapter methods------------------------------------------------------------------------------------


    /** Holds child views for one row. */
    private static class ProductViewHolder {

        private CheckBox checkBox;
        private TextView textView;
        private TextView priceTextView;

        public ProductViewHolder() {}

        public ProductViewHolder(TextView textView, CheckBox checkBox ,TextView priceTextView) {
            this.checkBox = checkBox ;
            this.textView = textView ;
            this.priceTextView = priceTextView;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

        public void setCheckBox(CheckBox checkBox) {
            this.checkBox = checkBox;
        }

        public TextView getPriceTextView() {
            return priceTextView;
        }

        public void setPriceTextView(TextView textView) {
            this.priceTextView = textView;
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }

    }



    /** Custom adapter for displaying an array of product objects. */
    private static class ProductArrayAdapter extends ArrayAdapter<Product> {

        private LayoutInflater inflater;

        public ProductArrayAdapter( Context context, List<Product> products_list ) {
            super(context, R.layout.simplerow, R.id.rowTextView, products_list );
            inflater = LayoutInflater.from(context) ;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // product to display
            Product product = (Product) this.getItem( position );

            // The child views in each row.
            CheckBox checkBox ;
            TextView textView ;
            TextView priceTextView;

            // Create a new row view
            if ( convertView == null ) {
                convertView = inflater.inflate(R.layout.simplerow, null);

                // Find the child views.
                textView = (TextView) convertView.findViewById( R.id.rowTextView );
                checkBox = (CheckBox) convertView.findViewById( R.id.CheckBox01 );
                priceTextView = (TextView) convertView.findViewById( R.id.price_text_view);

                // set object product view holder for this row to optimize and reuse the text view etc..
                convertView.setTag( new ProductViewHolder(textView, checkBox, priceTextView) );

                // Tag the CheckBox with the product it is displaying
                checkBox.setTag(product);
            }
            // Reuse existing row view
            else {
                // Because we use a ViewHolder, we avoid having to call findViewById().
                ProductViewHolder viewHolder = (ProductViewHolder) convertView.getTag();
                checkBox = viewHolder.getCheckBox() ;
                textView = viewHolder.getTextView() ;
                priceTextView = viewHolder.getPriceTextView();
            }

            // Display product data
            checkBox.setChecked( product.isChecked() );
            textView.setText(product.getName());
            priceTextView.setText(Double.toString(product.getPrice())+"$");
            return convertView;
        }

    }















    //----------------------------------------------------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onDestroy(){
        super.onDestroy();
        stopService(new Intent(this, UpdateService.class));
    }
}
