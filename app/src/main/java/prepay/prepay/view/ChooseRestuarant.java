package prepay.prepay.view;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import prepay.prepay.R;
import prepay.prepay.util.Manager;


public class ChooseRestuarant extends ActionBarActivity implements AdapterView.OnItemClickListener, TextWatcher,LocationListener {

    private ListView listview;
    private EditText inputSearch;
    private StableArrayAdapter adapter;
    private boolean joinTable=false;
    private ArrayList<String> listOfRestuarants;
    private ProgressDialog progress;
    Manager manager;



    private LocationManager locationManager;
    private String provider;



    protected void onCreate(Bundle savedInstanceState) {

        progress = new ProgressDialog(this);
        progress.setMessage("טוען רשימת מסעדות..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_restuarant);

        manager = Manager.getInstance();
        listview = (ListView) findViewById(R.id.rest_list_view);

        //check if the user asked to join table
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            joinTable = extras.getBoolean("joinTable");
        }

        String [] params = {manager.getUserName(),manager.getUserName()};
        //send rest list query to server
        manager.sendMessage(4,params,this);

    }




    public void  inflateList(String [] restList){

        //add all rest name to array list for the adapter
        final ArrayList<String> list = new ArrayList<String>();
        list.addAll(Arrays.asList(restList) );

        //make the adapter and set the list view to connect with it
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
        inputSearch = (EditText) findViewById(R.id.inputSearch);


        //the user click on parameter in list view - what to do
        listview.setOnItemClickListener(this);

        //Enabling Search Filter
        inputSearch.addTextChangedListener(this);

        progress.dismiss();

    }






    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final String item = (String) parent.getItemAtPosition(position);

        //set button text to show the resturant you chose
        if (!joinTable)
        {
            displayDialog(view, item);
        }
        else
        {
            displayJoinDialog(view,item);
        }
    }







    public void displayJoinDialog(View view, String restuarant_name){

        FragmentManager fragmentManager = getFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString("restName",restuarant_name);

        DialogJoinTable dialog = new DialogJoinTable();

        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "Join Table");
    }




    /**
     * display dialog which authenticate you know that you choose a specified rest, pass you to generate code
     */
    public void displayDialog(View view, String restuarant_name){

        FragmentManager fragmentManager = getFragmentManager();

        Bundle bundle = new Bundle();

        bundle.putString("restName",restuarant_name);

        //TODO:the rest name is not the correct parameter to base on

        DialogChooseNickName dialog = new DialogChooseNickName();
        dialog.setArguments(bundle);

        dialog.show(fragmentManager, "Choose NickName Dialog");
    }




    /**
     * when the input search text is change this metod is called and fillterd accordingly
     * @param s
     * @param start
     * @param before
     * @param count
     */
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        adapter.getFilter().filter(s);
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    //-------------------------------------------------------------location0--------------------
    @Override
    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
        Toast.makeText(this, String.valueOf(lat)+","+String.valueOf(lng), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
//-------------------------------------------------------------end location---------------------

    /**
     * stable array adapter to list view
     */
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        //Builder
        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        public boolean hasStableIds() {
            return true;
        }

    }




}
