package prepay.prepay.view;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import prepay.prepay.R;


public class DialogThanksForPaying extends DialogFragment implements View.OnClickListener {

    public Button backBtn;
    public Button continueBtn;
    public TextView txtView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {

        View myView = inflater.inflate(R.layout.dialog_thanks_for_paying, null);

        getDialog().setTitle("Payment received!");

        boolean checkItAll = getArguments().getBoolean("all was payed");
        continueBtn = (Button) myView.findViewById(R.id.continue_btn_exit);
        backBtn= (Button) myView.findViewById(R.id.back_see_order);
        continueBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);


        if(checkItAll){
            getDialog().setTitle("Payment received!");
        }else{
            getDialog().setTitle("Payment received!");
        }

        setCancelable(false);
        return myView;
    }


    public void onClick(View v) {
        if (v.getId() == R.id.continue_btn_exit)
        {
            System.exit(0);
        }else
        {
            dismiss();
        }
    }
}
