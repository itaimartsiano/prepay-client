package prepay.prepay.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import prepay.prepay.R;
import prepay.prepay.util.Manager;

public class SignUp extends ActionBarActivity {

    Manager manager;
    private ProgressDialog progress;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_CLIENT_ID = "ASAskIDfU6YHF33CO9Ri2ZHdQyYT_kdF_vFsZYzhW3-ruTes1a15E_yYGyDjVcmgjNXKsTMwOFcrYB4C";
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .languageOrLocale("he_IL")
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Checkout")
            .rememberUser(true)
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        manager = Manager.getInstance();

        progress = new ProgressDialog(this);
        progress.setMessage("מבצע רישום לאפיקציה");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void signUp(View view){
        String privateName = ((TextView) findViewById(R.id.private_name)).getText().toString();
        String lastName = ((TextView) findViewById(R.id.last_name)).getText().toString();
        String userName = ((TextView) findViewById(R.id.user_name)).getText().toString();
        String password = ((TextView) findViewById(R.id.password)).getText().toString();
        String rePassword = ((TextView) findViewById(R.id.re_enter_Password)).getText().toString();
        String email = ((TextView) findViewById(R.id.email)).getText().toString();
        String phoneNumber = ((TextView) findViewById(R.id.phone_number)).getText().toString();

        if (password.equals(rePassword)){
            progress.show();
            manager.signUp(privateName, lastName,userName,password,email,phoneNumber,this);
        }
        else{
            Toast.makeText(this, "These passwords don't match ", Toast.LENGTH_LONG).show();
        }

    }




    public void signUpAnswer(boolean result){

        final TextView userName = (TextView) findViewById(R.id.user_name);
        final TextView password = (TextView) findViewById(R.id.password);

        if(result){
            manager.update_pref(userName.getText().toString(), password.getText().toString(), this);
            Intent chooseOrJoin = new Intent(getBaseContext(), ChooseOrJoin.class);
            progress.dismiss();
            startActivity(chooseOrJoin);                 //change content view to choose resturant
        }else {
            progress.dismiss();
            Toast.makeText(this, "Sorry, something went wrong with signing up", Toast.LENGTH_LONG).show();
        }

    }


//-----------------------------------------------------paypal sign up--------------------------------------------------



    public void signUpWithPayPal(View view){

        Intent intent = new Intent(SignUp.this, PayPalProfileSharingActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PayPalProfileSharingActivity.EXTRA_REQUESTED_SCOPES, getOauthScopes());

        startActivityForResult(intent, REQUEST_CODE_PROFILE_SHARING);

    }


    private PayPalOAuthScopes getOauthScopes() {

        //the scopes you ask to share with our application
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS, PayPalOAuthScopes.PAYPAL_SCOPE_PROFILE, PayPalOAuthScopes.PAYPAL_SCOPE_PHONE) );
        return new PayPalOAuthScopes(scopes);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharing", auth.toJSONObject().toString(4));
                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharing", authorization_code);

                        sendAuthorizationToServer(auth);
                        //TODO: neeeeeeeeeeeeeeeeeeeeeeedddd to add future pament autorize

                        Intent chooseOrJoin = new Intent(getBaseContext(), ChooseOrJoin.class);
                        startActivity(chooseOrJoin);                 //change content view to choose resturant

                    } catch (JSONException e) {
                        Log.e("ProfileSharing", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharing", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharing",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }



    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        manager.getConnectionHandler().SignUpWithPayPal(authorization.getAuthorizationCode());

    }



}
