package prepay.prepay.view;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import prepay.prepay.R;
import prepay.prepay.util.Manager;


public class MainActivity extends ActionBarActivity {

    private Manager manager;
    private ProgressDialog progress;
    View view = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = new ProgressDialog(this);
        progress.setMessage("מאמת..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);

        //get manager object
        manager = Manager.getInstance();

        //get user preferences
        manager.init_pref(this);

        //TODO:add checkbox - if was checked in the last login call SignIn method automatically

    }


    /**
     * this method is called by SignIn button in activity_main layout to sign in application
     * @param view - the parameter comes from sign in button
     */
    public void SignIn(View view){
        this.view = view;
        progress.show();
        ImageButton signInBtn = (ImageButton) findViewById(R.id.sigInBtn);
        signInBtn.setImageResource(R.drawable.signinon_btn);
        signInBtn.setBackgroundResource(R.drawable.signinon_btn);
        final TextView userName = (TextView) findViewById(R.id.userName);
        final TextView password = (TextView) findViewById(R.id.password);
        manager.verificate(userName.getText().toString(), password.getText().toString(), this);

    }

    public void verificationAnswer(boolean result){

        final TextView userName = (TextView) findViewById(R.id.userName);
        final TextView password = (TextView) findViewById(R.id.password);

        ImageButton signInBtn = (ImageButton) findViewById(R.id.sigInBtn);
        signInBtn.setImageResource(R.drawable.signinoff_btn);
        signInBtn.setBackgroundResource(R.drawable.signinoff_btn);

        if(result){
            manager.update_pref(userName.getText().toString(), password.getText().toString(), this);
            Intent chooseOrJoin = new Intent(getBaseContext(), ChooseOrJoin.class);
            progress.dismiss();
            ActivityOptions options = ActivityOptions.makeScaleUpAnimation(view, 0,
                    0, view.getWidth(), view.getHeight());
            startActivity(chooseOrJoin, options.toBundle());
            startActivity(chooseOrJoin);                 //change content view to choose resturant
        }else {
            progress.dismiss();
            Toast.makeText(this, "Sorry, cannot login\n wrong password or user name", Toast.LENGTH_LONG).show();
        }

    }
    

    /**
     * this method call by signUp text field to open the sign up screen
     * @param view
     */
    public void SignUp(View view){
        Intent signUp = new Intent(getBaseContext(), SignUp.class);
        startActivity(signUp);
    }











    //---------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
















//wait for click to continue
//continue to choose restuarant or continue to sign up
//final Button signIn = (Button) findViewById(R.id.sigInBtn);         //sign in button
//signIn.setOnClickListener(onClickListener);
//final TextView signUp = (TextView) findViewById(R.id.signUp);
//signUp.setOnClickListener(onClickListener);


/*
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.sigInBtn:

                    final TextView userName = (TextView) findViewById(R.id.userName);
                    final TextView password = (TextView) findViewById(R.id.password);

                    boolean agree = verification.verificate(userName.getText().toString(), password.getText().toString());

                    if(agree){
                        init.update_pref(userName.getText().toString(), password.getText().toString());
                        setContentView(R.layout.choose_or_join);                 //change content view to choose resturant
                    }else {
                        setContentView(R.layout.activity_main);
                    }

                    break;
                case R.id.signUp:
                    setContentView(R.layout.sign_up);
                    onResume();
                    break;
            }

        }
    };
    */