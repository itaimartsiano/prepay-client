package prepay.prepay.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;
import android.widget.Toast;

import prepay.prepay.R;

public class Initialize {

    public Initialize(){
    }

    public void init_pref(Activity activity) {
        SharedPreferences sp = activity.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        if (sp.contains("initialized")) {
            Toast.makeText(activity, "process profile", Toast.LENGTH_SHORT).show();      //throw message to screen
            TextView userName = (TextView) activity.findViewById(R.id.userName);
            TextView password = (TextView) activity.findViewById(R.id.password);
            SharedPreferences.Editor editor = sp.edit();
            String user_name = sp.getString("first_name", "");
            String pass = sp.getString("pass", "");
            userName.setText(user_name);
            password.setText(pass);
        }

    }

    public void update_pref(String userName, String password, Activity activity){
        SharedPreferences sp = activity.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        if (sp != null){
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("first_name", userName);
            editor.putString("pass", password);
            editor.putBoolean("initialized", true);
            editor.commit();
            //Toast.makeText(activity, "updated profile", Toast.LENGTH_SHORT).show(); //TODO:delete
        }
    }

}
