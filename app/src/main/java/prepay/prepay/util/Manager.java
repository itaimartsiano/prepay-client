package prepay.prepay.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;

import prepay.prepay.util.Connectivity.ConnectionHandler;

/**
 * the manager class, all the data and connection pass throw this object
 */
public class Manager {

    private static Manager manager = new Manager();
    private Settings settings;
    private Initialize init;
    private ConnectionHandler connectionHandler;

    private Manager(){
        settings = new Settings();
        init = new Initialize();
        connectionHandler = new ConnectionHandler();
    }



    /**
     * @return this manager object
     */
    public static Manager getInstance(){
        return manager;
    }


    public ConnectionHandler getConnectionHandler(){
        return connectionHandler;
    }




    /**
     * this method will verificate user name and password with the server by using another methods which responsible
     * of connection with the server
     * @param userName
     * @param password
     * @return true if approve else otherwise
     */
    public void verificate(String userName, String password,Context context) {
        settings.verificate(userName,password,context);
    }


    /**
     * update the user name and password in the pref file
     * @param userName
     * @param password
     * @param activity
     */
    public void update_pref(String userName, String password, Activity activity){
        init.update_pref(userName, password, activity);
    }


    /**
     * initialize the pref file for the first time //TODO:maybe can merge with update_pref method
     * @param activity
     */
    public void init_pref(Activity activity) {
        init.init_pref(activity);
    }


    /**
     * the method the make the sign up of customer in the server
     */
    public void signUp(String privateName,String lastName,String userName,String password,String email,String phoneNumber,Context context){
        settings.signUp(privateName, lastName,userName,password,email,phoneNumber,context);
    }



    public String sendMessage(int messageType, String [] params,Object context){
        switch (messageType)
        {
            case 0: ;
                connectionHandler.signUpMessage(params,(Context)context);
                break;

            case 1:
                connectionHandler.signInMessage(params,(Context)context);
                break;

            case 2:
                int version = Integer.parseInt(params[0]);
                String ans = connectionHandler.UpdateOrderDetails(version,params[1],params[2]);
                return ans;
            case 3:
                connectionHandler.openTableMessage(params,(Fragment)context);
                break;
            case 4:
                connectionHandler.getRestListMessage(params,(Context)context);
                break;

        }
            //TODO: bad castingggggg!!!!! from object to context and fragement
        return  null;

    }



    public void updateUsernameAndPassfromserver(String userName, String passFromServer){
        settings.updateUserNameAndPassFromServer(userName,passFromServer);
    }

    public String getUserName() {
        return settings.getUserName();
    }

    public String getPassFromServer() {
        return settings.getSessionPassword();
    }
}
