package prepay.prepay.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.TimeUtils;
import android.text.format.Time;

import java.security.Timestamp;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;

/**
 * Created by itai marts on 17/06/2015.
 * Singleton class
 */
public class Settings {

    private String userName;
    private String sessionPassword;
    private long loginTime;
    private String restName;
    private String tableName;
    private SharedPreferences sharedPreferences;


    public Settings() {
        userName = "systemInit";
        sessionPassword = "systemInit";
        Date date = new Date();
        loginTime = date.getTime();


    }



    /**
     * the function will verificate the user name with the server by calling another method
     * if the server return true the method will save the parameters in the correct fields and return true, else return false
     *
     * @param userName
     * @return true if verification success else false
     */
    public void verificate(String userName, String password, Context context) {
        if (userName != null && password != null) {
            this.userName = userName;
            this.sessionPassword = password;
            //insert parametrs into string array
            String params [] = {userName, password};
            Manager.getInstance().sendMessage(1, params, context);     //1 symbolize sign in message type
        }
    }




    public void signUp(String privateName,String lastName,String userName,String password,String email,String phoneNumber, Context context) {
        if (userName != null && password != null) {
            this.userName = userName;
            this.sessionPassword = password;
            //insert parametrs into string array
            String params [] = {privateName, lastName,userName,password,email,phoneNumber};
            Manager.getInstance().sendMessage(0, params, context);     //1 symbolize sign in message type
        }
    }

    public void updateUserNameAndPassFromServer(String userName, String passFromServer){
        this.userName = userName;
        this.sessionPassword = passFromServer;
    }


    public String getUserName() {
        return userName;
    }

    public String getSessionPassword() {
        return sessionPassword;
    }
}
