package prepay.prepay.util.Connectivity;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import prepay.prepay.util.Manager;
import prepay.prepay.view.ChooseRestuarant;
import prepay.prepay.view.DialogChooseNickName;
import prepay.prepay.view.MainActivity;

/**
 * Created by itai marts on 29/09/2015.
 */
public class OpenTableTask extends AsyncTask<String, Integer, String> {

    private Fragment fragment = null;
    private String openTableTaskURL = null;
    private char[] messageFromServer;
    private int read;
    private Manager manager = Manager.getInstance();


    public OpenTableTask(Fragment fragment, String OpenTableTaskURL){
        this.fragment = fragment;
        this.openTableTaskURL = OpenTableTaskURL;
        this.read = -1;
    }


    @Override
    protected void onPreExecute() {
    }


    @Override
    protected String doInBackground(String... params) {

        //building message to server
        JSONObject messageToServer = new JSONObject();
        try {
            messageToServer.put("user name", manager.getUserName());
            messageToServer.put("password", manager.getPassFromServer());
            messageToServer.put("restName", params[0]);
            messageToServer.put("nickname", params[1]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //send the message and get the answer
        Log.i("open table task: ", "user message - " + messageToServer.toString());
        String messageFromServer = sendMessage(messageToServer.toString());
        Log.i("open table task: ","server message - "+messageFromServer);

        return messageFromServer;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
    }


    @Override
    protected void onPostExecute(String answerFromServer) {

        boolean approved = false;

        try {
            //retrieve params from server answer
            JSONObject jsonMessage = new JSONObject(answerFromServer);
            approved = (boolean) jsonMessage.get("approved");

            //check the answer from server and update verification class object
            DialogChooseNickName  dialogChooseNickName = (DialogChooseNickName) fragment;

            //update UI
            if (approved){
                dialogChooseNickName.seeOrderDetails();
            }else{
                dialogChooseNickName.nicknameIsNotValid();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private String sendMessage(String messageToServer) {

        URL serverUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {
            //initialization of connection
            serverUrl = new URL(openTableTaskURL);
            connection = (HttpURLConnection) serverUrl.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
}
