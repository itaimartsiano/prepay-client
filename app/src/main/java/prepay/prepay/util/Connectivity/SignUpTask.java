package prepay.prepay.util.Connectivity;
//TODO:need to finish configurations
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import prepay.prepay.util.Manager;
import prepay.prepay.view.MainActivity;
import prepay.prepay.view.SignUp;

/**
 * Created by itai marts on 14/08/2015.
 */
public class SignUpTask extends AsyncTask<String, Integer, String> {

    private Context context = null;
    private String signUpURL = null;
    private char[] messageFromServer;
    private int read;


    public SignUpTask(Context context, String signUpURL){
        this.context = context;
        this.signUpURL = signUpURL;
        this.read = -1;
    }


    @Override
    protected void onPreExecute() {
    }


    @Override
    protected String doInBackground(String... params) {

        //building message to server, assume there is enough params
        JSONObject messageToServer = new JSONObject();
        try {
            String privateName = "";
            String lastName = "";
            String userName = "";
            String password = "";
            String email = "";
            String phoneNumber = "";


            messageToServer.put("private name", params[0]);
            messageToServer.put("last name", params[1]);
            messageToServer.put("user name", params[2]);
            messageToServer.put("password", params[3]);
            messageToServer.put("email", params[4]);
            messageToServer.put("phone number", params[5]);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //send the message and get the answer
        Log.i("SignInTask: ","user message - "+messageToServer.toString());
        String messageFromServer = sendMessage(messageToServer.toString());
        Log.i("SignInTask: ","server message - "+messageFromServer);

        return messageFromServer;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
    }


    @Override
    protected void onPostExecute(String answerFromServer) {

        boolean success = false;

        try {
            //retrieve params from server answer
            JSONObject jsonMessage = new JSONObject(answerFromServer);
            String passFromServer = (String) jsonMessage.get("session password");
            String userName = (String) jsonMessage.get("user name");
            success = (boolean) jsonMessage.get("success");

            //check the answer from server and update verification class object
            if (success){
                Manager.getInstance().updateUsernameAndPassfromserver(userName,passFromServer);
            }

            //update UI
            SignUp signUp = (SignUp) context;
            signUp.signUpAnswer(success);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private String sendMessage(String messageToServer) {

        URL serverUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {
            //initialization of connection
            serverUrl = new URL(signUpURL);
            connection = (HttpURLConnection) serverUrl.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }



}