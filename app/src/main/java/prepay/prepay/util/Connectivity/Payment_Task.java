package prepay.prepay.util.Connectivity;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import prepay.prepay.util.Manager;
import prepay.prepay.view.DialogChooseNickName;
import prepay.prepay.view.OrderDetails;

/**
 * Created by itai marts on 10/10/2015.
 */
public class Payment_Task extends AsyncTask<String, Integer, String> {

    private Context context = null;
    private String paymentTaskURL = null;
    private char[] messageFromServer;
    private int read;
    private Manager manager = Manager.getInstance();


    public Payment_Task(Context context, String PaymentTaskURL){
        this.context = context;
        this.paymentTaskURL = PaymentTaskURL;
        this.read = -1;
    }


    @Override
    protected void onPreExecute() {
    }


    @Override
    protected String doInBackground(String... params) {

        //building message to server
        JSONObject messageToServer = new JSONObject();
        try {
            messageToServer.put("user name", manager.getUserName());
            messageToServer.put("password", manager.getPassFromServer());
            messageToServer.put("restName", params[0]);
            messageToServer.put("table nickname", params[1]);
            messageToServer.put("product payed numbers", params[2]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //send the message and get the answer
        Log.i("payment task: ", "user message - " + messageToServer.toString());
        String messageFromServer = sendMessage(messageToServer.toString());
        Log.i("payment task: ","server message - "+messageFromServer);

        return messageFromServer;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
    }


    @Override
    protected void onPostExecute(String answerFromServer) {

        boolean success = false;

        try {
            //retrieve params from server answer
            JSONObject jsonMessage = new JSONObject(answerFromServer);
            success = (boolean) jsonMessage.get("success");

            //check the answer from server and update verification class object
            OrderDetails orderDetails = (OrderDetails) context;

            //update UI
            if (success){
                orderDetails.paymentRecievedSuccessfully();
            }else{
                //TODO: do some thing
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private String sendMessage(String messageToServer) {

        URL serverUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {
            //initialization of connection
            serverUrl = new URL(paymentTaskURL);
            connection = (HttpURLConnection) serverUrl.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
}
