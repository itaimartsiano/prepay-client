package prepay.prepay.util.Connectivity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import prepay.prepay.util.Manager;
import prepay.prepay.view.MainActivity;

/**
 * Created by itai marts on 14/08/2015.
 */
public class SignInTask extends AsyncTask<String, Integer, String> {

    private Context context = null;
    private String signInURL = null;
    private char[] messageFromServer;
    private int read;


    public SignInTask(Context context, String signInURL){
        this.context = context;
        this.signInURL = signInURL;
        this.read = -1;
    }


    @Override
    protected void onPreExecute() {
    }


    @Override
    protected String doInBackground(String... params) {

        //building message to server
        JSONObject messageToServer = new JSONObject();
        try {
            messageToServer.put("user name", params[0]);
            messageToServer.put("password", params[1]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //send the message and get the answer
        Log.i("SignInTask: ","user message - "+messageToServer.toString());
        String messageFromServer = sendMessage(messageToServer.toString());
        Log.i("SignInTask: ","server message - "+messageFromServer);

        return messageFromServer;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
    }


    @Override
    protected void onPostExecute(String answerFromServer) {

        boolean approved = false;

        try {
            //retrieve params from server answer
            JSONObject jsonMessage = new JSONObject(answerFromServer);
            String passFromServer = (String) jsonMessage.get("password from server");
            String userName = (String) jsonMessage.get("user name");
            approved = (boolean) jsonMessage.get("approved");

            //check the answer from server and update verification class object
            if (approved){
                Manager.getInstance().updateUsernameAndPassfromserver(userName,passFromServer);
            }

            //update UI
            MainActivity main = (MainActivity) context;
            main.verificationAnswer(approved);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    private String sendMessage(String messageToServer) {

        URL serverUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {
            //initialization of connection
            serverUrl = new URL(signInURL);
            connection = (HttpURLConnection) serverUrl.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }



}