package prepay.prepay.util.Connectivity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import prepay.prepay.util.Manager;
import prepay.prepay.view.MainActivity;

/**
 * Created by itai marts on 17/08/2015.
 */
public class UpdateOrderDetailsTask {

    private String messageUrl = null;
    private char[] messageFromServer;
    private int read;
    private Manager manager = Manager.getInstance();


    public UpdateOrderDetailsTask(String messageUrl){
        this.messageUrl = messageUrl;
        this.read = -1;
    }


    public String execute(Integer versionNum, String nickName, String restName) {

        //building message to server, assume there is enough params
        JSONObject messageToServer = new JSONObject();
        try {
            messageToServer.put("user name", manager.getUserName());
            messageToServer.put("password from server", manager.getPassFromServer());
            messageToServer.put("version", versionNum.intValue());
            messageToServer.put("nickName",nickName);
            messageToServer.put("restName",restName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //send the message and get the answer
        Log.i("Update Order Details T", "user message - " + messageToServer.toString());
        String messageFromServer = sendMessage(messageToServer.toString());
        Log.i("Update Order Details T","server message - "+messageFromServer);

        return messageFromServer;
    }



    private String sendMessage(String messageToServer) {

        URL serverUrl = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();

        try {
            //initialization of connection
            serverUrl = new URL(messageUrl);
            connection = (HttpURLConnection) serverUrl.openConnection();
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //write the message to server
            writer.write(messageToServer, 0, messageToServer.length());
            writer.flush();
            writer.close();

            //get the ans from server
            inputStream = connection.getInputStream();
            messageFromServer = new char[1024];

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((read = reader.read(messageFromServer)) > 0) {
                sb.append(messageFromServer, 0, read);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }



}
