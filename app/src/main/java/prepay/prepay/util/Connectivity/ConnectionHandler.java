package prepay.prepay.util.Connectivity;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;

import java.util.List;

import prepay.prepay.util.Connectivity.SignInTask;
import prepay.prepay.view.SignUp;

/**
 * Created by itai marts on 18/06/2015.
 */
public class ConnectionHandler {

    private String signInURL = "http://testapp-prepay2015.rhcloud.com/signin";
    private String signUpURL = "http://testapp-prepay2015.rhcloud.com/signup";
    private String UpdateOrderDetailsURL = "http://testapp-prepay2015.rhcloud.com/update";
    private String openTableConnectionURL = "http://testapp-prepay2015.rhcloud.com/openTable";
    private String getRestListURL = "http://testapp-prepay2015.rhcloud.com/getRestList";
    private String paymentTaskURL = "http://testapp-prepay2015.rhcloud.com/paymentRecieved";
    private String joinTableConnectionURL = "http://testapp-prepay2015.rhcloud.com/JoinTable";
    private String signUpWithPayPalURL = "http://testapp-prepay2015.rhcloud.com/SignUpWithPayPal";

    //private String signInURL = "http://192.168.1.17:8080/prepay2015/signin";
    //private String UpdateOrderDetailsURL = "http://testapp-prepay2015.rhcloud.com/update";


    public ConnectionHandler() {

    }


    public void signInMessage(String params[], Context context) {

        SignInTask verificateTask = new SignInTask(context, signInURL);

        //check number of params and execute the verification with the server
        if (params.length > 1) {
            verificateTask.execute(params[0], params[1]);
        } else Log.i("connection handler: ", "SignInMessage dont get enough params");

    }


    public void signUpMessage(String params[], Context context) {

        SignUpTask SignUp = new SignUpTask(context, signUpURL);

        //check number of params and execute the verification with the server
        if (params.length > 5) {
            SignUp.execute(params[0], params[1], params[2],params[3],params[4],params[5]);
        } else Log.i("connection handler: ", "SignInMessage dont get enough params");

    }


    public String UpdateOrderDetails(int versionNum, String nickName, String restName) {

        UpdateOrderDetailsTask updateTask = new UpdateOrderDetailsTask(UpdateOrderDetailsURL);

        //execute the update asked
        return updateTask.execute(versionNum, nickName,restName);

    }



    public void openTableMessage(String []params, Fragment fragment){

        OpenTableTask openTableConnection = new OpenTableTask(fragment, openTableConnectionURL);

        if (params.length>1){
            openTableConnection.execute(params[0],params[1]);
        }
        else Log.i("connection handler: ", "open Table func did not recieve enough params");

    }




    public void getRestListMessage(String params[], Context context) {

        GetRestListTask getRestListTask = new GetRestListTask(context, getRestListURL);

        //check number of params and execute the verification with the server
        if (params.length > 1) {
            getRestListTask.execute(params[0], params[1]);
        } else Log.i("connection handler: ", "getRestlist message dont get enough params");

    }


    public void doPayment(String params[], Context context) {

        Payment_Task doPaymentTask = new Payment_Task(context, paymentTaskURL);

        //check number of params and execute the verification with the server
        if (params.length > 2) {
            doPaymentTask.execute(params[0], params[1],params[2]);
        } else Log.i("connection handler: ", "do payment message dont get enough params");

    }



    public void joinTableTask(String []params, Fragment fragment){

        JoinTableTask joinTableTask = new JoinTableTask(fragment, joinTableConnectionURL);

        if (params.length>1){
            joinTableTask.execute(params[0],params[1]);
        }
        else Log.i("connection handler: ", "join Table func did not recieve enough params");

    }


    public void SignUpWithPayPal(String authCode){

        SignUpWithPayPalTask signUpWithPayPal = new SignUpWithPayPalTask(signUpWithPayPalURL);
        signUpWithPayPal.execute(authCode);

    }

}
