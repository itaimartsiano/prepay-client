package prepay.prepay.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import prepay.prepay.restuarantObjects.Product;
import prepay.prepay.util.Manager;
import prepay.prepay.view.OrderDetails;

/**
 * Created by itai marts on 18/08/2015.
 */
public class UpdateService extends Service {

    int version = 0;
    String nickName = "";
    String restName = "";
    boolean run = true;
    Manager manager = Manager.getInstance();
    int refreshInterval = 3000;


    //this method called by start service
    public int onStartCommand(Intent intent, int flags, int startId) {

        nickName = intent.getStringExtra("nickName");
        restName = intent.getStringExtra("restName");

        //while true call the update method ans sleep for 3 sec

        Log.i("Update Service", "started");

        //the run method of runnable
        Runnable runnable = new Runnable() {
            public void run() {
                while (run){
                    update();
                }
            }
        };

        new Thread(runnable).start();
        return Service.START_NOT_STICKY;
    }




    //this method will make one update call to server
    private void update(){

        String [] params = new String[] {String.valueOf(version), nickName, restName};

        //ask connection handler to contact server with version number to get json string
        String ansFromServer = manager.sendMessage(2,params,this);

        //check if there is a new version number
        try {
            JSONObject json = new JSONObject(ansFromServer);

            //if the version is not equal do
            if (json.getInt("version") != version){
                Log.i("update service","new version recieved");

                //parse string to hashmap and send the message to OrderDetails activity
                HashMap<Integer,Product> pr = parseStringToHash(ansFromServer);

                //send the hashmap to OrderDetails activity
                Intent messageToOrderDetails = new Intent(OrderDetails.broadcastFilter);
                messageToOrderDetails.putExtra("hash", pr);
                LocalBroadcastManager.getInstance(this).sendBroadcast(messageToOrderDetails);
            }else{
                //if equal - sleep for 5 seconds
                Thread.sleep(refreshInterval);
            }

        } catch (JSONException | InterruptedException e) {
            e.printStackTrace();
        }
    }



    public  HashMap<Integer,Product> parseStringToHash(String ansFromServer){
        HashMap<Integer,Product> pr = new HashMap<Integer,Product>();
        try {
            JSONObject json = new JSONObject(ansFromServer);
            version = json.getInt("version");
            int numOfProducts = json.getInt("num of products");

            for (int i=0; i<numOfProducts; i++){
                JSONObject jsonSon = new JSONObject((String)json.get(String.valueOf(i)));
                String productName = (String) jsonSon.get("product name");
                double price = jsonSon.getDouble("price");
                int productId = jsonSon.getInt("product_order_number");
                pr.put(productId,new Product(productName,price,productId));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pr;
    }



    public void onDestroy() {
        run = false;
        super.onDestroy();
        Log.i("Update Service","service destroyed");
    }


    public IBinder onBind(Intent intent) {
        return null;
    }
}
