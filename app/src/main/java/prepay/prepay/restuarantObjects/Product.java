package prepay.prepay.restuarantObjects;

/**
 * Created by itai marts on 18/08/2015.
 */
public class Product {

    private String name = "" ;
    private boolean checked = false;
    private double price;
    private int productId = 0;

    public Product( String name , double price,  int productId) {
        this.name = name ;
        this.price = price;
        this.productId = productId;
    }

    public int getProductId() {return productId;}

    public double getPrice() {return price;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String toString() {
        return name ;
    }

    public void toggleChecked() {
        checked = !checked ;
    }
}
